export default {
  getId(state){
    return state.id;
  },
  getToken(state){
    return state.tokenId;
  },
  isAuthenticated(state){
    return !!state.tokenId;
  },
  didAutoLogout(state){
    return state.didLogout;
  }
};