export default {
  setUserId(state,payload){
    state.id = payload.userId;
    state.tokenId = payload.token;
    state.didLogout = false;
  },
  setLogout(state){
    state.tokenExpiration = null;
    state.tokenId = null;
    state.id = null;
  },
  setDidLogout(state){
    state.didLogout = true;
  }
};