import actions from './actions';
import mutations from './mutations';
import getters from './getters';

const auth = {
    state(){
      return {
        id: null,
        tokenId: null,
        tokenExpiration:null,
        didLogout: false
      };
    },
  mutations,
  getters,
  actions
};

export default auth;