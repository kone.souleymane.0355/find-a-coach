let timer;
export default {
  async login(context,payload){

    return context.dispatch('auth',{
      ...payload,
      mode: 'login'
    });
  },
  async signup(context,payload){
    return context.dispatch('auth',{
      ...payload,
      mode: 'signup'
    });
  },
  async auth(context,payload){
    const mode = payload.mode;
    let url = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=api_key";
    if(mode === 'signup'){
      url = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=api_key";
    }
    const response = await fetch(url,
      {
        method: 'POST',
        body: JSON.stringify({
          email: payload.email,
          password:payload.password,
          returnSecureToken:true
        })
      });
    const responseData = await response.json();

    if(!response.ok){
      const error = new Error(responseData.message || 'Failed to fetch data. Check your login data');
      throw error;
    }
    const expireIn = +responseData.expiresIn * 1000;
    //const expireIn = 5000;
    const expirationDate = new Date().getTime() + expireIn;
    localStorage.setItem('tokenId',responseData.idToken);
    localStorage.setItem('id',responseData.localId);
    localStorage.setItem('expirationDate',expirationDate);

    timer = setTimeout(function(){

      context.dispatch('autoLogout');
    },expireIn)
    context.commit('setUserId',{
      token: responseData.idToken,
      userId: responseData.localId
    })

  },
  tryLogin(context){
    const token = localStorage.getItem('tokenId');
    const id = localStorage.getItem('id');
    const expiration = localStorage.getItem('expirationDate');
    const expireIn = +expiration - new Date().getTime();

    if(expireIn < 0){
      return;
    }
    console.log(expireIn);
    timer = setTimeout(function(){
      context.dispatch('autoLogout')
    },expireIn);
    if(token && id){
      context.commit('setUserId',{
        token,
        id
      })
    }
  },
  logout(context){
    localStorage.removeItem('id');
    localStorage.removeItem('tokenId');
    localStorage.removeItem('expirationDate');
    clearTimeout(timer);
    context.commit('setLogout');
  },
  autoLogout(context){

    context.dispatch('logout');
    context.commit('setDidLogout')
  }
};