export default {
  async addRequest(context, payload) {
    const newRequest = {
      id: new Date().toISOString(),
      coachId: payload.coachId,
      userEmail: payload.email,
      message: payload.message
    };

    const response = await fetch(`https://vue-http-demo-19055-default-rtdb.europe-west1.firebasedatabase.app/requests/${newRequest.coachId}.json`,{
      method: 'PUT',
      body: JSON.stringify(newRequest)
    });

    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.message || 'Failed to fetch!');
      throw error;
    }

    context.commit('addRequest',newRequest)
  },
  async loadRequests(context) {
    const token = context.rootGetters.getToken;

    const response = await fetch(`https://vue-http-demo-19055-default-rtdb.europe-west1.firebasedatabase.app/requests.json?auth=`+
    token);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(responseData.message || 'Failed to fetch!');
      throw error;
    }
    const requests = Object.keys(responseData).map(key => ({
      id: key,
      ...responseData[key]
    }));
    context.commit('setRequests',requests);
  }
};