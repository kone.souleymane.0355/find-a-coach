import getters from './getters';
import actions from './actions';
import mutations from './mutations';

const requests = {
  namespaced:true,
  state(){
    return {
      requests: []
    };
  },
  mutations,
  getters,
  actions,
};

export default requests;