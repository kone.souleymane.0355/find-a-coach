export default {
  getRequests(state,_,_2,rootGetters){
    const userId = rootGetters.getId;
    return state.requests.filter(ci => ci.coachId === userId);
  },
  hasRequest(_,getters){
    return getters.getRequests.length > 0;
  },
};