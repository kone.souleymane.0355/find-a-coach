export default {
  getCoaches(state){
    return state.coaches;
  },
  hasCoach(state){
    return state.coaches && state.coaches.length > 0;
  },
  isCoach(_,getters,_2,rootGetters){
    const coaches = getters.getCoaches;
    const  userId = rootGetters.getId;
    return coaches.some(coach => coach.id === userId);
  },
  shouldUpdateCoaches(state){
    const lastFech = state.lastFetch;
    if(!lastFech){
      return true;
    }

    const currentTimestamp = new Date().getTime();
    //console.log((currentTimestamp - lastFech) / 1000);
    return (currentTimestamp - lastFech) / 1000 > 60;
  }
}