 export default {
  async addNewCoach(context,payload){
    const userId =  context.rootGetters.getId;
    const newCoach = {
      id:userId,
      firstName:payload.firstName,
      lastName:payload.lastName,
      description:payload.description,
      hourlyRate:payload.hourlyRate,
      areas: payload.areas
    };
    const token = context.rootGetters.getToken;

    const response = await fetch(`https://vue-http-demo-19055-default-rtdb.europe-west1.firebasedatabase.app/coaches/${userId}.json?auth=`+
      token,{
      method: 'PUT',
      body: JSON.stringify(newCoach)
      
    });
  const responseData = await response.json()
    if (!response.ok){
      const error = new Error(responseData.message || 'Failed to fetch!');
      throw error;
    }
    context.commit('addCoach',{
      ...newCoach
    });
    context.commit('setLastFetchTimestamp');
  },
  async loadCoaches(context,payload){
    
    if (!payload.forceRefresh && context.getters.shouldUpdateCoaches){
      
      return;
    }
    
    const response = await fetch(`https://vue-http-demo-19055-default-rtdb.europe-west1.firebasedatabase.app/coaches.json`);
    const responseData = await response.json();
    if (!response.ok){
      const error = new Error(responseData.message || 'Failed to fetch!');
      throw error;
    }
    const coaches = Object.keys(responseData).map(key => ({
      id: key,
      ...responseData[key]
    }));
    context.commit('setCoaches',coaches);
  }
}