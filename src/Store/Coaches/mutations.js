export default {
  addCoach(state,payload){
    state.coaches.unshift(payload)
  },
  setCoaches(state,payload){
    state.coaches = payload
  },
  setLastFetchTimestamp(state){
    state.lastFetch = new Date().getTime();
  }
}