import { createStore } from 'vuex';
import coachModule from './Coaches/index.js';
import requestModule from './Requests/index.js';
import authModule from '@/Store/Auth';

const store = createStore({
  modules: {
    coach: coachModule,
    req: requestModule,
    auth:authModule
  }
});

export default store;