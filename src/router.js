import { createRouter,createWebHistory } from 'vue-router';

import NotFound from './components/pages/NotFound';
import CoachDetail from './components/pages/coaches/CoachDetail';
import CoachList from './components/pages/coaches/CoachList';
import CoachRegister from './components/pages/coaches/CoachRegister';
import ContactCoach from './components/pages/requests/ContactCoach';
import RequestsReceived from './components/pages/requests/RequestsReceived';
import UserAuth from './components/pages/auth/UserAuth';
import  store from './Store/index';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path:'/',
      redirect: '/coaches'
    },
    {
      path: '/coaches',
      component:CoachList
    },
    {
      path: '/register',
      component:CoachRegister,
      meta: {requireAuth:true}
    },
    {
      path: "/auth",
      component: UserAuth,
      meta: {requireUnauth:true}
    },
    {
      path: '/coaches/:id',
      props:true,
      component:CoachDetail,
      children:[
        {
          path:'contact',
          component:ContactCoach
        }
      ]
    },
    {
      path: '/requests',
      component:RequestsReceived,
      meta: {requireAuth:true}
    },
    {
      path:'/:notFound(.*)',
      component: NotFound
    }

  ],
});

router.beforeEach(function(to,_,next){
  if (to.meta.requireAuth && !store.getters.isAuthenticated){
    next('/auth');
  }else if(to.meta.requireUnauth && store.getters.isAuthenticated){
    next('/coaches')
  }else {
    next();
  }
});
export default router;